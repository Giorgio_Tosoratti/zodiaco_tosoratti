<?php

    echo "Inserire mese di nascita: ";
    $handle = fopen("php://stdin", "r");
    $temp = fgets($handle);

do
{
    echo "Inserire giorno di nascita: ";
    $handle = fopen ("php://stdin","r");
    $line = fgets($handle);
}while (trim($line)<1 || trim($line)>31);

do {
    echo "Nascita:\n-Ora: ";
    $handle = fopen("php://stdin", "r");
    $ora = fgets($handle);
}while($ora < 0 || $ora > 23);
do {
    echo "-Minuti: ";
    $handle = fopen("php://stdin", "r");
    $minuti = fgets($handle);
}while($minuti < 0 || $minuti > 59);

do {
    echo "Dove sei nato in Italia? Nord = N, Centro = C, Sud = S\n";
    $handle = fopen("php://stdin", "r");
    $luogo = fgets($handle);
}while(trim($luogo) != N && trim($luogo) != C && trim($luogo) != S );

if(trim($luogo) == N)
    if($minuti<20) {
        $minuti = 40 + $minuti;
        $ora = $ora - 1;
    }
    else $minuti =  $minuti - 20;
else if(trim($luogo) == C)
    if($minuti<20) {
        $minuti = 50 + $minuti;
        $ora = $ora - 1;
    }
    else $minuti =  $minuti - 10;

if($minuti + $tempoSiderale > 59)
{
    $minuti = ($minuti + $tempoSiderale) % 60;
    $ora = $ora + 1;
}

if($ora >= 24)
    $ora = $ora - 24;

if(trim($luogo))

if((trim($temp) == 'Dicembre' && trim($line) > 21) || (trim($temp) == 'Gennaio' && trim($line) < 21)) {
    echo 'Capricorno';
    $tempoSiderale = 6.5;
            }
else if((trim($temp) == 'Gennaio' && trim($line) > 20) || (trim($temp) == 'Febbraio' && trim($line) < 20)) {
    echo 'Acquario';
    $tempoSiderale = 8.5;
            }
if((trim($temp) == 'Febbraio' && trim($line) > 19) || (trim($temp) == 'Marzo' && trim($line) < 21)) {
    echo 'Pesci';
    $tempoSiderale = 10.5;
            }
else if((trim($temp) == 'Marzo' && trim($line) > 20) || (trim($temp) == 'Aprile' && trim($line) < 21)) {
    echo 'Ariete';
    $tempoSiderale = 12.5;
            }
if((trim($temp) == 'Aprile' && trim($line) > 20) || (trim($temp) == 'Maggio' && trim($line) < 21)) {
    echo 'Toro';
    $tempoSiderale = 14.5;
            }
else if((trim($temp) == 'Maggio' && trim($line) > 20) || (trim($temp) == 'Giugno' && trim($line) < 22)) {
    echo 'Gemelli';
    $tempoSiderale = 16.5;
            }
if((trim($temp) == 'Giugno' && trim($line) > 21) || (trim($temp) == 'Luglio' && trim($line) < 23)) {
    echo 'Cancro';
    $tempoSiderale = 18.5;
            }
else if((trim($temp) == 'Luglio' && trim($line) > 22) || (trim($temp) == 'Agosto' && trim($line) < 24)) {
    echo 'Leone';
    $tempoSiderale = 20.5;
            }
if((trim($temp) == 'Agosto' && trim($line) > 23) || (trim($temp) == 'Settembre' && trim($line) < 23)) {
    echo 'Vergine';
    $tempoSiderale = 22.5;
            }
else if((trim($temp) == 'Settembre' && trim($line) > 22) || (trim($temp) == 'Ottobre' && trim($line) < 23)) {
    echo 'Bilancia';
    $tempoSiderale = 0.5;
            }
if((trim($temp) == 'Ottobre' && trim($line) > 22) || (trim($temp) == 'Novembre' && trim($line) < 23)) {
    echo 'Scorpione';
    $tempoSiderale = 2.5;
            }
else if((trim($temp) == 'Novembre' && trim($line) > 22) || (trim($temp) == 'Dicembre' && trim($line) < 22)) {
    echo 'Sagittario';
    $tempoSiderale = 4.5;
            }

echo ' ascendente ';

    $orario = $ora . ":" . $minuti;

if((substr($orario,0,2) > 12 && substr($orario,3,4) > 53) && (substr($orario,0,2) < 16 && substr($orario,3,4) < 44))
    echo "Capricorno";
else if((substr($orario,0,2) > 14 && substr($orario,3,4) > 43) && (substr($orario,0,2) < 18 && substr($orario,3,4) < 01))
    echo "Acquario";
if((substr($orario,0,2) > 16 && substr($orario,3,4) > 0) && (substr($orario,0,2) < 19 && substr($orario,3,4) < 01))
    echo "Pesci";
else if((substr($orario,0,2) > 17 && substr($orario,3,4) > 0) && (substr($orario,0,2) < 19 && substr($orario,3,4) < 60))
    echo "Ariete";
if((substr($orario,0,2) > 18 && substr($orario,3,4) > -1) && (substr($orario,0,2) < 21 && substr($orario,3,4) < 18 ))
    echo "Toro";
else if((substr($orario,0,2) > 19 && substr($orario,3,4) > 17) && (substr($orario,0,2) < 23 && substr($orario,3,4) < 9))
    echo "Gemelli";
if((substr($orario,0,2) > 21 && substr($orario,3,4) > 8 ) && (substr($orario,0,2) < 1 && substr($orario,3,4) < 35))
    echo "Cancro";
else if((substr($orario,0,2) > -1 && substr($orario,3,4) > 34) && (substr($orario,0,2) < 4 && substr($orario,3,4) < 18))
    echo "Leone";
if((substr($orario,0,2) > 2 && substr($orario,3,4) > 17) && (substr($orario,0,2) < 7 && substr($orario,3,4) < 1))
    echo "Vergine";
else if((substr($orario,0,2) > 5 && substr($orario,3,4) > 0) && (substr($orario,0,2) < 9 && substr($orario,3,4) < 44))
    echo "Bilancia";
if((substr($orario,0,2) > 7 && substr($orario,3,4) > 43) && (substr($orario,0,2) < 12 && substr($orario,3,4) < 26))
    echo "Scorpione";
else if((substr($orario,0,2) > 10 && substr($orario,3,4) > 25) && (substr($orario,0,2) < 14 && substr($orario,3,4) < 54))
    echo "Sagittario";

?>